'use strict';

let model = document.getElementById("model")
let brand = document.getElementById("brand")
let reg = document.getElementById("license-plate")
let prod = document.getElementById("production-year")
let kilometers = document.getElementById("kilometers")
let table = document.getElementById("table")
let btn = document.getElementById("btn-submit")
let error = document.getElementById("error")

const regex = /^[a-zA-Z]{2}-\d{3}-[a-zA-Z]{2}$/;

class Car {
    constructor(model, brand, reg, prod, kilometers) {
        this.model = model;
        this.brand = brand;
        this.reg = reg;
        this.prod = prod;
        this.kilometers = kilometers;
    }
}

class ListOfCars {
    constructor() {
        this.cars = JSON.parse(localStorage.getItem("cars")) || [];
    }
    addCar(model, brand, reg, prod, kilometers) {
        let validPlate = regex.test(reg);
        if (model === "") {
            error.innerHTML = "Invalid input!"
        } else if (brand === "") {
            error.innerHTML = "Invalid input!"
        } else if (validPlate === "") {
            error.innerHTML = "Invalid input!"
        } else if (prod === "") {
            error.innerHTML = "Invalid input!"
        } else if (kilometers === "") {
            error.innerHTML = "Invalid input!"
        } else {
            const newCars = new Car(model, brand, reg, prod, kilometers);
            this.cars.push(newCars);
            this.showCars();
            localStorage.setItem("cars", JSON.stringify(this.cars));
            this.resetForm();
        }
    }
    editCars(index) {
        const car = this.cars[index];
        model.value = car.model;
        brand.value = car.brand;
        prod.value = car.prod;
        reg.value = car.reg;
        kilometers.value = car.kilometers;
        btn.textContent = "Edit";
        btn.setAttribute("name", index);
    }
    saveChanges(index) {
        const car = this.cars[index];
        car.model = model.value;
        car.brand = brand.value;
        car.prod = prod.value;
        car.reg = reg.value;
        car.kilometers = kilometers.value;
        this.showCars();
        localStorage.setItem("cars", JSON.stringify(this.cars));
        this.resetForm();
    }
    deleteCar(index) {
        this.cars.splice(index, 1);
        this.showCars();
        localStorage.setItem("cars", JSON.stringify(this.cars));
    }
    showCars() {
        let a = "";
        for (let i = 0; i < this.cars.length; i++) {
            const car = this.cars[i];
            let state;
            const year = new Date().getFullYear();
            if (car.prod > (year - 3) || car.kilometers < 3000) {
                state = "New";
            } else {
                state = "Used car"
            } let k = "";
            a += `<tr id=${k}>
                     <td>${i + 1}</td>
                     <td>${car.brand}</td>
                     <td>${car.model}</td>
                     <td>${car.reg}</td>
                     <td>${car.prod}</td>
                     <td>${car.kilometers}</td>
                     <td>${state}</td>
                     <td><button class="actions" onclick="car.editCars(${i})" alt= "Edit">✏</button>
                     <button class="actions" onclick="car.deleteCar(${i})">🗑</button><br/></td>    
                    </tr>
                `;
        }
        table.innerHTML = a;
    }
    resetForm() {
        btn.textContent = "Park";
        btn.removeAttribute("name");
        model.value = "";
        brand.value = "";
        reg.value = "";
        prod.value = "";
        kilometers.value = "";
        error.innerHTML = "";
    }
}

const car = new ListOfCars();

if (car.cars.length > 0) {
    car.showCars();
}

btn.addEventListener("click", (event) => {
    event.preventDefault();

    if (btn.textContent === "Park") {
        car.addCar(model.value, brand.value, reg.value, prod.value, kilometers.value)
    } else if (btn.textContent === "Edit") {
        const index = parseInt(btn.getAttribute("name"));
        car.saveChanges(index);
    }
});