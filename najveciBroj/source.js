let maxDisplayed = document.querySelector(".maxDisplay");
let maxCalculated = document.getElementById("maxCalculated");
let maxNumber = 0;
let number1 = document.getElementById("num1");
let number2 = document.getElementById("num2");
let number3 = document.getElementById("num3");

maxCalculated.addEventListener("click", function(){
    let num1 = Number(number1.value);
    let num2 = Number(number2.value);
    let num3 = Number(number3.value);
    let arr = [num1, num2, num3];

    let maxNumber = arr[0];
    for (let i = 1; i < arr.length; i++) {
        if(arr[i] > maxNumber)
            maxNumber = arr[i];
    }
    maxDisplayed.textContent = maxNumber;

});

resetBtn.addEventListener("click", function(){
    location.reload()
});
